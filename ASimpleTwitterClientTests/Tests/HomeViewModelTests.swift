//
//  HomeViewModelTests.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import ASimpleTwitterClient

class HomeViewModelTests: XCTestCase {
    
    var timelineService: MocTwitterTimelineService!
    var favoriteService: MocTwitterFavoriteService!
    var homeViewModel: HomeViewModel!
    
    override func setUp() {
        super.setUp()
        timelineService = MocTwitterTimelineService()
        favoriteService = MocTwitterFavoriteService()
        homeViewModel = HomeViewModel(service: timelineService, favoritableService: favoriteService)
    }
    
    override func tearDown() {
        homeViewModel = nil
        super.tearDown()
    }
    
    func testFetchData_ReloadSuccess() {
        let expectation = XCTestExpectation(description: "Fetch tweets with reloading")
        homeViewModel.reloadCallback = { reload, error in
            XCTAssertNil(error, "Should reload tweets")
            expectation.fulfill()
        }
        
        homeViewModel.fetchData(reload: true)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testFetchData_ReloadError() {
        let expectation = XCTestExpectation(description: "Fetch tweets with reloading")
        timelineService.simulateServerError = true
        homeViewModel.reloadCallback = { reload, error in
            XCTAssertNotNil(error, "Should received error")
            expectation.fulfill()
        }
        
        homeViewModel.fetchData(reload: true)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testFetchData_ReloadTweetsCount() {
        let expectation = XCTestExpectation(description: "Fetch tweets with reloading")
        timelineService.simulateServerError = false
        let count = 10
        homeViewModel.pageSize = count
        
        homeViewModel.reloadCallback = { [weak self] reload, error in
            XCTAssertTrue(self?.homeViewModel.tweetsCount() == count, "Should received error")
            expectation.fulfill()
        }
        
        homeViewModel.fetchData(reload: true)
        
        wait(for: [expectation], timeout: 1.0)
    }

    func testFetchData_Paging() {
        let expectation = XCTestExpectation(description: "Fetch tweets with reloading")
        timelineService.simulateServerError = false
        let count = 10
        homeViewModel.pageSize = count
        
        homeViewModel.reloadCallback = { [weak self] reload, error in
            if reload == false {
                XCTAssertTrue(self?.homeViewModel.tweetsCount() == 2 * count, "Should received \(2 * count) tweets")
                expectation.fulfill()
            }
        }
        
        homeViewModel.fetchData(reload: true)
        homeViewModel.fetchData(reload: false)

        wait(for: [expectation], timeout: 1.0)
    }
}
