//
//  ASimpleTwitterClientTests.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import ASimpleTwitterClient

class ComposeViewModelTests: XCTestCase {
    
    var composeViewModel: ComposeViewModel!
    
    override func setUp() {
        super.setUp()
        composeViewModel = ComposeViewModel(service: MocTwitterTweetService())
        composeViewModel.reset()
    }
    
    override func tearDown() {
        composeViewModel = nil
        super.tearDown()
    }
    
    func testShouldChangeText_ShouldChange() {
        let currentText = StubTweetTexts.shortText
        let replacementText = StubTweetTexts.shortText
        let range = NSRange(location: 0, length: 0)
        
        let result = composeViewModel.shouldChangeText(currentText, in: range, replacementText: replacementText)
        
        XCTAssertTrue(result, "Should shange text")
    }

    func testShouldChangeText_ShouldNotChange() {
        let currentText = StubTweetTexts.shortText
        let replacementText = StubTweetTexts.tooLongText
        let range = NSRange(location: 1, length: 3)
        
        let result = composeViewModel.shouldChangeText(currentText, in: range, replacementText: replacementText)
        
        XCTAssertFalse(result, "Should not change text")
    }
    
    func testSetTweetText_ShouldSet() {
        let tweetText = StubTweetTexts.shortText
        
        let result = composeViewModel.setTweetText(tweetText)
        
        XCTAssertTrue(result, "Should be able to change text")
    }

    func testSetTweetText_CheckText() {
        let tweetText = StubTweetTexts.shortText
        
        _ = composeViewModel.setTweetText(tweetText)
        
        XCTAssertTrue(tweetText == composeViewModel.tweetText, "Should be able to change text")
    }

    func testSetTweetText_ShouldNotSet() {
        let tweetText = StubTweetTexts.tooLongText
        
        let result = composeViewModel.setTweetText(tweetText)
        
        XCTAssertFalse(result, "Should not be able to change text")
    }

    func testSetTweetText_ShouldSetNil() {
        let tweetText: String? = nil
        
        let result = composeViewModel.setTweetText(tweetText)
        
        XCTAssertTrue(result, "Should be able to change text")
    }
    
    func testSendTweet_Success() {
        let expectation = XCTestExpectation(description: "Send tweet to the server")
        
        let tweetText = StubTweetTexts.shortText
        _ = composeViewModel.setTweetText(tweetText)
        composeViewModel.didSendTweetCallback = { error in
            XCTAssertNil(error, "Should send tweet text")
            expectation.fulfill()
        }
        composeViewModel.sendTweet()
        
        wait(for: [expectation], timeout: 1.0)
    }

    func testSendTweet_Error() {
        let expectation = XCTestExpectation(description: "Send tweet to the server")
        
        let tweetText = StubTweetTexts.shortText
        _ = composeViewModel.setTweetText(tweetText)
        composeViewModel.didSendTweetCallback = { error in
            XCTAssertNil(error, "Should send tweet text")
            expectation.fulfill()
        }
        composeViewModel.sendTweet()
        
        wait(for: [expectation], timeout: 1.0)
    }
}
