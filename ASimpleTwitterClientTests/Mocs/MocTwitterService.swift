//
//  MocTwitterServicable.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
@testable import ASimpleTwitterClient

class MocTwitterService {
    
    var simulateServerError: Bool = false
    var simulateTweetsCount: Int = 10
    
    func processTweets(callback: TwitterSwifterable.TweetsCallback) {
        if simulateServerError {
            let error = NSError(domain: "ServiceDomain", code: 404, userInfo: nil)
            callback(nil, error)
        } else {
            let tweets = StubTweet.sharedInstance.tweets(count: simulateTweetsCount)
            callback(tweets, nil)
        }
    }
    
    func processSingleTweet(callback: TwitterSwifterable.SingleTweetCallback) {
        if simulateServerError {
            let error = NSError(domain: "ServiceDomain", code: 404, userInfo: nil)
            callback(nil, error)
        } else {
            let tweet = StubTweet.sharedInstance.singleTweet()
            callback(tweet, nil)
        }
    }
    
}
