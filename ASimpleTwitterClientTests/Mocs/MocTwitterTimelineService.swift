//
//  MocTwitterTimelineService.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
@testable import ASimpleTwitterClient

class MocTwitterTimelineService: MocTwitterService, TwitterTimelinable {
    
    func homeTimeline(count: Int?, sinceID: String?, maxID: String?, callback: @escaping TweetsCallback) {
        processTweets(callback: callback)
    }

}
