//
//  MocTwitterFavoriteService.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
@testable import ASimpleTwitterClient

class MocTwitterFavoriteService: MocTwitterService, TwitterFavoritable {
    
    func favoritesTweet(userID: String, count: Int?, sinceID: String?, maxID: String?, callback: @escaping TweetsCallback) {
        processTweets(callback: callback)
    }
    
    func favoriteTweet(tweetID: String, callback: @escaping SingleTweetCallback) {
        processSingleTweet(callback: callback)
    }
    
    func unfavoriteTweet(tweetID: String, callback: @escaping SingleTweetCallback) {
        processSingleTweet(callback: callback)
    }
}
