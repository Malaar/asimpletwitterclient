//
//  MocTwitterTweetable.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

@testable import ASimpleTwitterClient

class MocTwitterTweetService: MocTwitterService, TwitterTweetable {

    func sendTweet(status: String, callback: @escaping SingleTweetCallback) {
        processSingleTweet(callback: callback)
    }

}
