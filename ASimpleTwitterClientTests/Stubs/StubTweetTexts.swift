//
//  StubTweetTexts.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

struct StubTweetTexts {
    
    static let shortText = "Aliquam ullamcorper nisi in arcu suscipit sollicitudin"
    static let tooLongText = """
    Fusce non facilisis ante. Donec tempor ullamcorper nibh eu tempor.
    Nam dictum, magna vel rutrum egestas, lectus felis ultrices lectus, non tempor nisi urna at lorem.
    Suspendisse in porta lorem, ac placerat lorem. Ut a sem neque. Fusce non ultricies risus.
    Aliquam efficitur auctor magna, ut pellentesque urna pharetra sit amet.
    Phasellus commodo luctus erat volutpat vulputate. Sed odio turpis, condimentum a placerat sed, cursus ut libero.
    Vivamus sed nibh augue. Nunc quam nulla, rutrum at ipsum vitae, accumsan lacinia ligula.
    """
}
