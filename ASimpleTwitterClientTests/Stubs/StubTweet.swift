//
//  StubTweet.swift
//  ASimpleTwitterClientTests
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import TwitterKit

class StubTweet {
    
    static let sharedInstance = StubTweet()
    
    func tweets(count: Int) -> [TWTRTweet]? {
        return Array<TWTRTweet>(repeating: self.tweetPrototype, count: count)
    }

    func singleTweet() -> TWTRTweet? {
        return tweetPrototype
    }
    
    private let tweetPrototype: TWTRTweet = {
        let bundle = Bundle(for: StubTweet.self)
        let filePath = bundle.path(forResource: "TweetSample", ofType: "json")
        let fileURL = URL(fileURLWithPath: filePath!)
        let data = try! Data(contentsOf: fileURL, options: .mappedIfSafe)
        let object = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String: AnyObject]
        return TWTRTweet(jsonDictionary: object)!
    }()
}
