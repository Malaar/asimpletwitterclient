//
//  TWTRTwitter.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

extension TWTRTwitter {
    
    static func isUserAuthorized() -> Bool {
        let session = self.sharedInstance().sessionStore.session()
        return session != nil
    }
    
    static func logoutCurrentUser() {
        guard let session = self.sharedInstance().sessionStore.session() else { return }
        self.sharedInstance().sessionStore.logOutUserID(session.userID)
    }
}
