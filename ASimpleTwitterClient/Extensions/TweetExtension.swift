//
//  TWTRTweet.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/15/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

extension Array where Array.Element == TWTRTweet {
    
    func indexOfTweet(_ tweet: TWTRTweet) -> Int? {
        return self.index { $0.tweetID == tweet.tweetID }
    }
}
