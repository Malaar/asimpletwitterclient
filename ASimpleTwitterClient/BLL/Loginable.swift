//
//  Loginable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

protocol Loginable {
    
    typealias LoginCallback = (Error?) -> Void
    
    func login(callback: LoginCallback?)
}

extension Loginable {
    
    func login(callback: LoginCallback? = nil) {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if let session = session {
                print("Signed in as \(session.userName)")
                callback?(nil)
            } else {
                print("Error: \(String(describing: error?.localizedDescription))")
                callback?(error)
            }
        }
    }
}
