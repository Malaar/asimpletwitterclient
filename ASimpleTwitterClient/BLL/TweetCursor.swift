//
//  TweetCursor.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

struct TweetCursor {
    let minPosition: String
    let maxPosition: String
    
    var preMinPosition: String? {
        var result: String? = nil
        if var value = Int(minPosition) {
            value -= 1
            result = String(value)
        }
        return result
    }
}

extension TweetCursor {
    
    static func cursorFromTweetBatch(_ tweets: [TWTRTweet]?) -> TweetCursor? {
        var cursor: TweetCursor? = nil
        if let tweets = tweets, !tweets.isEmpty {
            cursor = TweetCursor(minPosition: tweets.last!.tweetID,
                                 maxPosition: tweets.first!.tweetID)
        }
        return cursor
    }
}
