//
//  HomeViewModel.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit


//MARK: - FavoritesViewModel

class HomeViewModel: TweetViewModelCollection {
    
    private let service: TwitterTimelinable
    
    init(service: TwitterTimelinable, favoritableService: TwitterFavoritable) {
        self.service = service
        super.init(favoritableService: favoritableService)

        NotificationCenter.default.addObserver(self, selector: #selector(processTweetFavoriteStatusNotification(_:)),
                                               name: TweetFavoriteStatusChangedNotification, object: nil)
    }

    override func fetchData(reload: Bool) -> Void {
        var maxID: String? = nil
        if !reload {
            maxID = currentCursor?.preMinPosition
        }
        
        service.homeTimeline(count: pageSize, sinceID: nil, maxID: maxID) { [weak self] (tweets, error) in
            if let tweets = tweets {
                if reload { self?.tweets = tweets } else { self?.tweets.append(contentsOf: tweets) }
                self?.currentCursor = TweetCursor.cursorFromTweetBatch(self?.tweets)
                self?.reloadCallback?(reload, nil)
            }
            else {
                self?.tweets = []
                self?.currentCursor = nil
                self?.reloadCallback?(reload, error)
            }
        }
    }


    //MARK: - Notifications
    
    @objc private func processTweetFavoriteStatusNotification(_ notification: Notification) {
        guard let tweet = notification.userInfo?[TweetFavoriteStatusChangedTweetKey] as? TWTRTweet else { return }
        if let index = tweets.indexOfTweet(tweet) {
            self.tweets[index] = tweet
            tweetDidChangeCallback?(index)
        }
    }
}
