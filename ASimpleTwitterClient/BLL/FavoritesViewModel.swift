//
//  FavoritesViewModel.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit


//MARK: - FavoritesViewModel

class FavoritesViewModel: TweetViewModelCollection {
    
    private let service: TwitterFavoritable
    private let userID: String
    
    init(service: TwitterFavoritable, userID: String) {
        self.service = service
        self.userID = userID
        super.init(favoritableService: service)
        
        NotificationCenter.default.addObserver(self, selector: #selector(processTweetFavoriteStatusNotification(_:)),
                                               name: TweetFavoriteStatusChangedNotification, object: nil)
    }

    override func fetchData(reload: Bool) -> Void {
        var maxID: String? = nil
        if !reload {
            maxID = currentCursor?.preMinPosition
        }
        service.favoritesTweet(userID: userID, count: pageSize, sinceID: nil, maxID: maxID) { [weak self] (tweets, error) in
            if let tweets = tweets {
                if reload { self?.tweets = tweets } else { self?.tweets.append(contentsOf: tweets) }
                self?.currentCursor = TweetCursor.cursorFromTweetBatch(self?.tweets)
                self?.reloadCallback?(reload, nil)
            }
            else {
                self?.tweets = []
                self?.currentCursor = nil
                self?.reloadCallback?(reload, error)
            }
        }
    }
    

    //MARK: - Notifications
    
    @objc private func processTweetFavoriteStatusNotification(_ notification: Notification) {
        guard let tweet = notification.userInfo?[TweetFavoriteStatusChangedTweetKey] as? TWTRTweet else { return }
        if !tweet.isLiked, let index = tweets.indexOfTweet(tweet) {
            tweets.remove(at: index)
            if let tweetDidRemoveCallback = tweetDidRemoveCallback {
                tweetDidRemoveCallback(index)
            } else {
                reloadCallback?(true, nil)
            }
        } else if tweet.isLiked && !tweets.contains(tweet) {
            tweets.append(tweet)
            tweets.sort { Int($0.tweetID)! > Int($1.tweetID)! }
            reloadCallback?(true, nil)
        }
    }
}
