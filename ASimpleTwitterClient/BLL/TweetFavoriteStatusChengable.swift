//
//  TweetFavoriteStatusChengable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/15/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

let TweetFavoriteStatusChangedNotification = Notification.Name(rawValue: "TweetFavoriteStatusChangedNotification")
let TweetFavoriteStatusChangedTweetKey = "tweet"

protocol TweetFavoriteStatusChengable {
    
    typealias TweetFavoriteStatusCallback = (Error?) -> Void
    
    /**
     * Call this method to change tweet favorite status.
     * Method can send Notification 'TweetFavoriteStatusChangedNotification' to inform about result of status changing
     * resulting tweet sould be available in userInfo via 'TweetFavoriteStatusChangedTweetKey' key
     **/
    func changeFavoriteStatus(tweet: TWTRTweet, callback: TweetFavoriteStatusCallback?) -> Void
}
