//
//  Logoutable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

protocol Logoutable {
    
    typealias LogoutCallback = () -> Void
    
    func logout(callback: LogoutCallback?)
}

extension Logoutable {
    
    func logout(callback: LogoutCallback? = nil) {
        TWTRTwitter.logoutCurrentUser()
        callback?()
    }
}
