//
//  TweetCollectableViewModel.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

class TweetViewModelCollection {
    
    var tweets = [TWTRTweet]()
    var currentCursor: TweetCursor?    
    var pageSize: Int = 30
    var reloadCallback: ReloadCallback?
    var tweetDidChangeCallback: TweetDidChangeCallback?
    var tweetDidRemoveCallback: TweetDidRemoveCallback?

    private var favoritableService: TwitterFavoritable! = nil

    init(favoritableService: TwitterFavoritable) {
        self.favoritableService = favoritableService
        NotificationCenter.default.addObserver(self, selector: #selector(tweetDidSentNotification(notification:)), name: TweetDidSentNotification, object: nil)
    }

    func fetchData(reload: Bool) -> Void {
        fatalError("Method doesn't implemented. Subclass should implement it!")
    }
    
    
    //MARK: - Notifications
    
    @objc private func tweetDidSentNotification(notification: Notification) {
        guard let tweet = notification.userInfo?[TweetFavoriteStatusChangedTweetKey] as? TWTRTweet else { return }
        if tweets.indexOfTweet(tweet) == nil {
            tweets.append(tweet)
            tweets.sort { Int($0.tweetID)! > Int($1.tweetID)! }
            reloadCallback?(true, nil)
        }
    }
}


//MARK: TweetViewModelCollectable protocol

extension TweetViewModelCollection: TweetViewModelCollectable {
    
    func tweetsCount() -> Int {
        return tweets.count
    }
    
    func tweet(at indexPath: IndexPath) -> TWTRTweet? {
        return indexPath.row < tweets.count ? tweets[indexPath.row] : nil
    }
}


//MARK: TweetFavoriteStatusChengable protocol

extension TweetViewModelCollection: TweetFavoriteStatusChengable {
    
    func changeFavoriteStatus(tweet: TWTRTweet, callback: TweetFavoriteStatusCallback? = nil) -> Void {
        guard let index = tweets.indexOfTweet(tweet) else {
            print("WARNING: Collection doesn't contain tweet \(tweet.tweetID). Favorite status won't change")
            return
        }
        let oldTweet = tweet
        tweets[index] = tweet.withLikeToggled()     // update tweet to display correct favorite status after cell reusing
        
        let serviceCallback: TwitterSwifterable.SingleTweetCallback = { [weak self] (receivedTweet, error) in
            guard let oldIndex = self?.tweets.indexOfTweet(oldTweet) else {
                print("NOTE: Tweet was already deleted from collection. Nothing to update")
                return
            }
            if let error = error { print("ERROR: \(error)") }
            let tweet = receivedTweet ?? oldTweet
            self?.tweets[oldIndex] = tweet
            NotificationCenter.default.post(name: TweetFavoriteStatusChangedNotification,
                                            object: nil,
                                            userInfo: [TweetFavoriteStatusChangedTweetKey: tweet])
            callback?(error)
        }
        if tweet.isLiked {
            favoritableService.unfavoriteTweet(tweetID: tweet.tweetID, callback: serviceCallback)
        } else {
            favoritableService.favoriteTweet(tweetID: tweet.tweetID, callback: serviceCallback)
        }
    }
}
