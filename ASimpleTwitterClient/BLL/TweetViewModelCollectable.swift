//
//  TweetViewModelCollectable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

protocol TweetViewModelCollectable: TweetFavoriteStatusChengable, Logoutable {
    
    typealias ReloadCallback = (Bool, Error?) -> Void
    typealias TweetDidChangeCallback = (Int) -> Void    // Int - index of tweet which was changed
    typealias TweetDidRemoveCallback = (Int) -> Void    // Int - index of tweet which was removed

    var reloadCallback: ReloadCallback? {get set}
    var tweetDidChangeCallback: TweetDidChangeCallback? { get set}
    var tweetDidRemoveCallback: TweetDidRemoveCallback? { get set}
    
    var pageSize: Int {get set}

    func tweetsCount() -> Int
    func tweet(at indexPath: IndexPath) -> TWTRTweet?
    func fetchData(reload: Bool) -> Void    
}
