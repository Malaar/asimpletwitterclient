//
//  ComposeViewModel.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

let TweetDidSentNotification = Notification.Name(rawValue: "TweetDidSentNotification")
let TweetDidSentNotificationTweetKey = "tweet"

fileprivate struct Constants {
    
    static let twitterMaximumCharacters = 280
}

class ComposeViewModel {
    
    typealias EnableSendingTweetCallback = (Bool) -> Void
    typealias LettersNumberCallback = (String) -> Void
    typealias WillSendTweetCallback = () -> Void
    typealias DidSendTweetCallback = (Error?) -> Void
    
    private let service: TwitterTweetable

    var enableSendingTweetCallback: EnableSendingTweetCallback?
    var lettersNumberCallback: LettersNumberCallback?
    var willSendTweetCallback: WillSendTweetCallback?
    var didSendTweetCallback: DidSendTweetCallback?

    private(set) var tweetText: String? {
        didSet {
            enableSendingTweetCallback?(canSendTweet())
            lettersNumberCallback?(String(availableLettersCount()))
        }
    }
    
    required init(service: TwitterTweetable) {
        self.service = service
    }

    func reset() {
        tweetText = nil
    }
    
    func setTweetText(_ text: String?) -> Bool {
        guard let text = text else {
            tweetText = nil
            return true
        }
        if text.count <= Constants.twitterMaximumCharacters {
            self.tweetText = text
            return true
        }
        return false
    }
    
    func shouldChangeText(_ currentText: String?, in range: NSRange, replacementText text: String) -> Bool {
        let currentText = currentText ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let newText = currentText.replacingCharacters(in: stringRange, with: text)
        return newText.count <= Constants.twitterMaximumCharacters
    }
    
    func sendTweet() {
        if canSendTweet() {
            willSendTweetCallback?()
            service.sendTweet(status: tweetText!) { [weak self] tweet, error in
                if let tweet = tweet {
                    let userInfo = [TweetDidSentNotificationTweetKey: tweet]
                    NotificationCenter.default.post(name: TweetDidSentNotification, object: nil, userInfo: userInfo)
                }
                self?.didSendTweetCallback?(error)
            }
        }
    }
    
    private func canSendTweet() -> Bool {
        return (tweetText ?? "").count > 0
    }
    
    private func availableLettersCount() -> Int {
        return Constants.twitterMaximumCharacters - (tweetText ?? "") .count
    }
}
