//
//  Injector.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/16/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Swinject
import SwifteriOS
import TwitterKit

final class Injector {
    
    static let defaultContainer: Container = {
        let container = Container()
        container.register(Swifter.self) { _ in
            guard let session = TWTRTwitter.sharedInstance().sessionStore.session() else {
                fatalError("Twitter session isn't instantiated!")
            }
            let config = TWTRTwitter.sharedInstance().sessionStore.authConfig
            return Swifter(consumerKey: config.consumerKey,
                           consumerSecret: config.consumerSecret,
                           oauthToken: session.authToken,
                           oauthTokenSecret: session.authTokenSecret)
        }
        container.register(TwitterTimelinable.self) { r in
            let swifter = r.resolve(Swifter.self)!
            return TwitterTimelineService(swifter: swifter)
        }
        container.register(TwitterFavoritable.self) { r in
            let swifter = r.resolve(Swifter.self)!
            return TwitterFavoriteService(swifter: swifter)
        }
        container.register(TwitterTweetable.self) { r in
            let swifter = r.resolve(Swifter.self)!
            return TwitterTweetService(swifter: swifter)
        }
        container.register(HomeViewModel.self) { resolver in
            let service1 = resolver.resolve(TwitterTimelinable.self)!
            let service2 = resolver.resolve(TwitterFavoritable.self)!
            return HomeViewModel(service: service1, favoritableService: service2)
        }
        container.register(FavoritesViewModel.self) { resolver in
            guard let session = TWTRTwitter.sharedInstance().sessionStore.session() else {
                fatalError("Twitter session isn't instantiated!")
            }
            let service = resolver.resolve(TwitterFavoritable.self)!
            return FavoritesViewModel(service: service, userID: session.userID)
        }
        container.register(ComposeViewModel.self) { resolver in
            let service = resolver.resolve(TwitterTweetable.self)!
            return ComposeViewModel(service: service)
        }
        return container
    }()
    
    private init() { }
}
