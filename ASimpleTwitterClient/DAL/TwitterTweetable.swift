//
//  TweetComposable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/15/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

/**
 * Operations with tweets
 */
protocol TwitterTweetable: TwitterSwifterable {
    
    func sendTweet(status: String, callback: @escaping SingleTweetCallback)
}
