//
//  TwitterFavoritesService.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import SwifteriOS


//MARK: - TwitterFavoriteService

class TwitterFavoriteService {
    
    private let swifter: Swifter
    
    required init(swifter: Swifter) {
        self.swifter = swifter
    }
}


//MARK: - TwitterFavoritable protocol

extension TwitterFavoriteService: TwitterFavoritable {
    
    func favoritesTweet(userID: String, count: Int? = 20, sinceID: String?, maxID: String?, callback: @escaping TweetsCallback) {
        let userTag = UserTag.id(userID)
        swifter.getRecentlyFavoritedTweets(for: userTag, count: count, sinceID: sinceID, maxID: maxID, tweetMode: .default, success: {
            [weak self] (json, rawData) in
            let jsonData = rawData as! Data
            do {
                let tweets = try self?.rawTweetsToTweetObjects(jsonData: jsonData)
                callback(tweets, nil)
            } catch {
                callback(nil, error)
            }
        }, failure: { error in
            callback(nil, error)
        })
    }
    
    func favoriteTweet(tweetID: String, callback: @escaping SingleTweetCallback) {
        swifter.favoriteTweet(forID: tweetID, includeEntities: nil, tweetMode: .default,
                              success: self.singleTweetSuccessHandler(callback: callback), failure: { callback(nil, $0) })
    }

    func unfavoriteTweet(tweetID: String, callback: @escaping SingleTweetCallback) {
        swifter.unfavoriteTweet(forID: tweetID, includeEntities: nil, tweetMode: .default,
                                success: self.singleTweetSuccessHandler(callback: callback), failure: { callback(nil, $0) })
    }
    
}
