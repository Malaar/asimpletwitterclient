//
//  TwitterTimelinable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

/**
 * Operations with timelines
 */
protocol TwitterTimelinable: TwitterSwifterable {
    
    func homeTimeline(count: Int?, sinceID: String?, maxID: String?, callback: @escaping TweetsCallback)
}
