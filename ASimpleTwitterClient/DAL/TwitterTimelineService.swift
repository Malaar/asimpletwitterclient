//
//  TwitterTimelineService.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import SwifteriOS


//MARK: - TwitterTimelineService

class TwitterTimelineService {
    
    private let swifter: Swifter
    
    required init(swifter: Swifter) {
        self.swifter = swifter
    }
}


//MARK: - TwitterTimelinable protocol

extension TwitterTimelineService: TwitterTimelinable {
    
    func homeTimeline(count: Int? = 30, sinceID: String? = nil, maxID: String? = nil, callback: @escaping TweetsCallback) {
        swifter.getHomeTimeline(count: count, sinceID: sinceID, maxID: maxID,
                                success: self.tweetsSuccessHandler(callback: callback), failure: { callback(nil, $0) })
    }
}
