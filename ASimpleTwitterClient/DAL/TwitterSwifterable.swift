//
//  TwitterSwifterableService.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit
import SwifteriOS

protocol TwitterSwifterable: class {
    
    typealias TweetsCallback = ([TWTRTweet]?, Error?) -> Void
    typealias SingleTweetCallback = (TWTRTweet?, Error?) -> Void

    func rawTweetsToTweetObjects(jsonData: Data) throws -> [TWTRTweet]
    func rawSingleTweetToTweetObjects(jsonData: Data) throws -> TWTRTweet?
}

extension TwitterSwifterable {
    
    func rawTweetsToTweetObjects(jsonData: Data) throws -> [TWTRTweet] {
        let objects = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! [Dictionary<AnyHashable, Any>]
        let tweets = objects.compactMap { TWTRTweet(jsonDictionary: $0) }
        return tweets
    }

    func rawSingleTweetToTweetObjects(jsonData: Data) throws -> TWTRTweet? {
        let object = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! Dictionary<AnyHashable, Any>
        let tweets = TWTRTweet(jsonDictionary: object)
        return tweets
    }

    func tweetsSuccessHandler(callback: @escaping TweetsCallback) -> Swifter.SuccessHandler {
        let handler: Swifter.SuccessHandler = { [weak self] (json, rawData) in
            let jsonData = rawData as! Data
            do {
                let tweets = try self?.rawTweetsToTweetObjects(jsonData: jsonData)
                callback(tweets, nil)
            } catch {
                callback(nil, error)
            }
        }
        return handler
    }

    func singleTweetSuccessHandler(callback: @escaping SingleTweetCallback) -> Swifter.SuccessHandler {
        let handler: Swifter.SuccessHandler = { [weak self] (json, rawData) in
            let jsonData = rawData as! Data
            do {
                let tweet = try self?.rawSingleTweetToTweetObjects(jsonData: jsonData)
                callback(tweet, nil)
            } catch {
                callback(nil, error)
            }
        }
        return handler
    }
}
