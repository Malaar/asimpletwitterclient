//
//  TweetComposeService.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/15/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import SwifteriOS


//MARK: - TweetComposeService

class TwitterTweetService {
    
    private let swifter: Swifter
    
    required init(swifter: Swifter) {
        self.swifter = swifter
    }
}


//MARK: - TwitterTweetable protocol

extension TwitterTweetService: TwitterTweetable {
    
    func sendTweet(status: String, callback: @escaping SingleTweetCallback) {
        swifter.postTweet(status: status, mediaIDs: [String](),
                          success: self.singleTweetSuccessHandler(callback: callback),
                          failure: { callback(nil, $0) })
    }
}
