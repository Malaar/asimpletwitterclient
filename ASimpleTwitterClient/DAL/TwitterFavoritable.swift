//
//  TwitterFavoritable.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

/**
 * Operations with favorites
 */
protocol TwitterFavoritable: TwitterSwifterable {
    
    func favoritesTweet(userID: String, count: Int?, sinceID: String?, maxID: String?, callback: @escaping TweetsCallback)
    func favoriteTweet(tweetID: String, callback: @escaping SingleTweetCallback)
    func unfavoriteTweet(tweetID: String, callback: @escaping SingleTweetCallback)
}
