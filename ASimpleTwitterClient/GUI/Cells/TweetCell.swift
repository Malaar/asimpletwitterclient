//
//  TweetCell.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit

class TweetCell: UICollectionViewCell {
    
    struct Constants {
        
        static let reuseIdentifier = "TweetCell"
        static let tweetStyle = TWTRTweetViewStyle.compact
        static let tweetShowActionButtons = false
        fileprivate static let favoriteButtonImageName = "Favorites"
        fileprivate static let favoriteButtonSize = CGSize(width: 30, height: 30)
        fileprivate static let favoriteButtonLeftInset = 60
    }
    
    private var tweetView: TWTRTweetView! = nil
    private var favoriteButton: UIButton! = nil
    var favoriteToggleCallback: ((TWTRTweet) -> Void)! = nil

    //MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.contentView.backgroundColor = UIColor.white
        
        self.tweetView = TWTRTweetView(tweet: nil, style: Constants.tweetStyle)
        self.contentView.addSubview(tweetView)
        tweetView.theme = .light
        tweetView.showBorder = false
        tweetView.showActionButtons = Constants.tweetShowActionButtons
        tweetView.translatesAutoresizingMaskIntoConstraints = false
        tweetView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        tweetView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        tweetView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        
        favoriteButton = UIButton(type: .system)
        self.contentView.addSubview(favoriteButton)
        favoriteButton.frame = CGRect(origin: CGPoint.zero, size: Constants.favoriteButtonSize)
        favoriteButton.contentMode = .scaleAspectFill
        favoriteButton.setImage(UIImage(named: Constants.favoriteButtonImageName), for: .normal)
        favoriteButton.translatesAutoresizingMaskIntoConstraints = false
        favoriteButton.topAnchor.constraint(equalTo: tweetView.bottomAnchor).isActive = true
        favoriteButton.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor,
                                                constant: CGFloat(Constants.favoriteButtonLeftInset)).isActive = true
        favoriteButton.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
        favoriteButton.addTarget(self, action: #selector(favoriteButtonPressed(sender:)), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Configuration
    
    func configure(with tweet: TWTRTweet?) {
        tweetView.configure(with: tweet)
        favoriteButton.isUserInteractionEnabled = true
        if let tweet = tweet {
            configureFavoriteButtonStatus(tweet: tweet)
        }
    }
    
    func configureFavoriteButtonStatus(tweet: TWTRTweet) {
        favoriteButton.tintColor = tweet.isLiked ? GUITheme.default.favoriteTweetButtonColor : GUITheme.default.unfavoriteTweetButtonColor
    }
    

    //MARK: - Actions

    @objc private func favoriteButtonPressed(sender: UIButton) {
        let oldTweet = tweetView.tweet
        configure(with: oldTweet.withLikeToggled())
        favoriteButton.isUserInteractionEnabled = false
        favoriteToggleCallback?(oldTweet)
    }
    
    
    //MARK: - Cell height calculation

    static func heightForTweet(_ tweet: TWTRTweet, style: TWTRTweetViewStyle, fittingWidth: CGFloat, showActions: Bool) -> CGFloat {
        return Constants.favoriteButtonSize.height + TweetViewSizeCalculator.heightForTweet(tweet, style: style, fittingWidth: fittingWidth, showActions: showActions)
    }
}
