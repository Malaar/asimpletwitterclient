//
//  ViewController.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit
import SwifteriOS
import PKHUD


//MARK: - HomeController

class HomeController: TweetCollectionController {

    override func createViewModel() -> TweetViewModelCollectable? {
        return Injector.defaultContainer.resolve(HomeViewModel.self)
    }
    
    
    //MARK: - Configuration
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        viewModel?.tweetDidChangeCallback = {[weak self] (tweetIndex) in
            self?.collectionView?.reloadItems(at: [IndexPath(row: tweetIndex, section: 0)])
        }
    }
}


//MARK: - Actions

extension HomeController {
    
    @IBAction private func logoutPressed(_ sender: Any) {
        viewModel?.logout {
            Director.sharedInstance.switchUI(isUserAuthorized: TWTRTwitter.isUserAuthorized())
        }
    }

    @IBAction func composeControllerWillClosed(segue: UIStoryboardSegue) {
    }    
}
