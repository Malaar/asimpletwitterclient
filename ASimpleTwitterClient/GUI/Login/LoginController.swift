//
//  LoginController.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit

class LoginController: UIViewController {
    
    private let viewModel = LoginViewModel()
}


//MARK: - Actions

extension LoginController {
    
    @IBAction private func loginButtonPressed(_ sender: Any) {
        viewModel.login { [unowned self] error in
            if error != nil {
                let alertController = UIAlertController(title: "Authorization failure",
                                                        message: "Something went wrong during authorization",
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            } else {
                Director.sharedInstance.switchUI(isUserAuthorized: TWTRTwitter.isUserAuthorized())
            }
        }
    }
    
    @IBAction func aboutControllerWillClosed(segue: UIStoryboardSegue) {        
    }
}
