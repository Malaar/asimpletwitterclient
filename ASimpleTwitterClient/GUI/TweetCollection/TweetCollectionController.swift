//
//  TweetCollectionController.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit
import SwifteriOS
import PKHUD


//MARK: - Constants

fileprivate struct Constants {
    
    struct CollectionLayout {
        
        struct Phone {
            static let itemMargin: CGFloat = 10.0
            static let columnsNumberPortrait = 1
            static let columnsNumberLandscape = 2
        }
        
        struct Pad {
            static let itemMargin: CGFloat = 20.0
            static let columnsNumberPortrait = 3
            static let columnsNumberLandscape = 4
        }
        
        static func itemMargin(traitCollection: UITraitCollection) -> CGFloat {
            return traitCollection.userInterfaceIdiom == .phone ? Phone.itemMargin : Pad.itemMargin
        }
        
        static func columnsNumber(traitCollection: UITraitCollection) -> Int {
            if traitCollection.userInterfaceIdiom == .phone {
                return UIScreen.main.isPortraitOrientation ? Phone.columnsNumberPortrait : Phone.columnsNumberLandscape
            } else {
                if traitCollection.horizontalSizeClass == .compact {
                    return Pad.columnsNumberPortrait
                } else {
                    return UIScreen.main.isPortraitOrientation ? Pad.columnsNumberPortrait : Pad.columnsNumberLandscape
                }
            }
        }
    }
}


//MARK: - TweetCollectionController

class TweetCollectionController: UICollectionViewController {
    
    var viewModel: TweetViewModelCollectable? = nil

    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = createViewModel()
        setupViewModelCallbacks()
        setupCollection()
        fetchData(reload: true)
    }
    
    
    //MARK: - Configuration
    
    func createViewModel() -> TweetViewModelCollectable? { return nil }
    
    func setupViewModelCallbacks() {
        viewModel?.reloadCallback = { [weak self] (reload, error) in
            if (self?.collectionView?.refreshControl?.isRefreshing) != nil {
                self?.collectionView?.refreshControl?.endRefreshing()
            }
            if(error != nil) {
                HUD.flash(.error, onView: self?.view, delay: 1.0, completion: nil)
            } else {
                if reload {
                    HUD.hide(animated: true)
                }
                self?.collectionView?.reloadData()
            }
        }
    }
    
    func setupCollection() {
        self.collectionView?.refreshControl = UIRefreshControl()
        self.collectionView?.refreshControl?.tintColor = GUITheme.default.orangeColor
        self.collectionView?.refreshControl?.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.backgroundColor = .groupTableViewBackground
        self.collectionView?.register(TweetCell.self, forCellWithReuseIdentifier: TweetCell.Constants.reuseIdentifier)
    }
    
    private func setupCallbacksForCell(_ cell: TweetCell) {
        cell.favoriteToggleCallback = { [weak self] tweet in
            self?.viewModel?.changeFavoriteStatus(tweet: tweet, callback: nil)
        }
    }
    
    
    //MARK: - Data management
    
    @objc private func reloadData() {
        fetchData(reload: true)
    }
    
    private func fetchData(reload: Bool) {
        if reload {
            HUD.show(.progress, onView: self.view)
        }
        viewModel?.fetchData(reload: reload)
    }
}


//MARK: - UICollectionViewDataSource

extension TweetCollectionController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.tweetsCount() ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TweetCell.Constants.reuseIdentifier , for: indexPath) as! TweetCell
        let tweet = viewModel?.tweet(at: indexPath) ?? nil
        cell.configure(with: tweet)
        setupCallbacksForCell(cell)
        return cell
    }
    
}


//MARK: - UICollectionViewDelegate

extension TweetCollectionController {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let viewModel = viewModel, indexPath.row == viewModel.tweetsCount() - 1 {
            viewModel.fetchData(reload: false)
        }
    }
}


//MARK: - UICollectionViewDelegateFlowLayout

extension TweetCollectionController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columnsNumber = Constants.CollectionLayout.columnsNumber(traitCollection: self.traitCollection)
        
        var margin = collectionView.contentInset.left + collectionView.contentInset.right
        margin += collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
        margin += Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection) * (CGFloat(columnsNumber) - 1)
        
        var itemSize = collectionView.bounds.size
        itemSize.width = ((itemSize.width - margin) / CGFloat(columnsNumber)).rounded(.down)
        
        if let tweet = viewModel?.tweet(at: indexPath) {
            itemSize.height = TweetCell.heightForTweet(tweet, style: TweetCell.Constants.tweetStyle, fittingWidth: itemSize.width, showActions: TweetCell.Constants.tweetShowActionButtons)
        } else {
            print("WARNING: viewModel is nil!")
        }
        
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }
}
