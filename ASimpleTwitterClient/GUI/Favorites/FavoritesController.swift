//
//  FavoritesController.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit
import SwifteriOS
import PKHUD


//MARK: - FavoritesController

class FavoritesController: TweetCollectionController {

    override func createViewModel() -> TweetViewModelCollectable? {
        return Injector.defaultContainer.resolve(FavoritesViewModel.self)
    }
    
    
    //MARK: - Configuration
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        viewModel?.tweetDidChangeCallback = { [weak self] (tweetIndex) in
            self?.collectionView?.reloadItems(at: [IndexPath(row: tweetIndex, section: 0)])
        }
    }
}


//MARK: - Actions

extension FavoritesController {
    
    @IBAction func logoutPressed(_ sender: Any) {
        viewModel?.logout {
            Director.sharedInstance.switchUI(isUserAuthorized: TWTRTwitter.isUserAuthorized())
        }
    }

    @IBAction func composeControllerWillClosed(segue: UIStoryboardSegue) {
    }    
}
