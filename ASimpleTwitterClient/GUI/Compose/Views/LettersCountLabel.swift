//
//  LabelColorized.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/15/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit

class LettersCountLabel: UILabel {

    @IBInspectable var warningTextColor: UIColor?
    @IBInspectable var defaultTextColor: UIColor?
    
    override var text: String? {
        didSet {
            if let text = text, let number = Int(text), number <= 0 {
                textColor = warningTextColor
            } else {
                textColor = defaultTextColor
            }
        }
    }
}
