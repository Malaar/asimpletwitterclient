//
//  ComposeController.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit
import SwifteriOS
import PKHUD


// MARK: - Constants

fileprivate struct Constants {
    
    static let closeSegueIdentifier = "closeComposeControllerSegue"
}


// MARK: - ComposeController

class ComposeController: UIViewController {
    
    let viewModel: ComposeViewModel = Injector.defaultContainer.resolve(ComposeViewModel.self)!
    
    @IBOutlet private weak var tweetBarButtonItem: UIBarButtonItem!
    @IBOutlet private weak var tweetTextView: UITextView!
    @IBOutlet private weak var lettersNumerLabel: UILabel!
    @IBOutlet private weak var bottomConstraint: NSLayoutConstraint!


    //MARK: - Configuration
    
    func setupViewModelCallbacks() {
        viewModel.enableSendingTweetCallback = { [weak self] enableSendTweet in
            self?.tweetBarButtonItem.isEnabled = enableSendTweet
        }
        viewModel.lettersNumberCallback = { [weak self] lettersNumber in
            self?.lettersNumerLabel.text = lettersNumber
        }
        viewModel.willSendTweetCallback = { [weak self] in
            self?.tweetTextView.resignFirstResponder()
            HUD.show(.progress)
        }
        viewModel.didSendTweetCallback = { error in
            if let error = error {
                HUD.flash(.error, delay: 1.0)
                print("WARNING: Can't send tweet: \(error)")
            } else {
                HUD.flash(.success, delay: 0.5)
                self.performSegue(withIdentifier: Constants.closeSegueIdentifier, sender: self)
            }
        }
    }

    func configureKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
    }

    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModelCallbacks()
        viewModel.reset()
        configureKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tweetTextView.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tweetTextView.resignFirstResponder()
    }
    
    @objc func keyboardWasShown(notification: Notification) {
        let info = notification.userInfo
        let value = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let size = value.cgRectValue.size
        self.bottomConstraint.constant = size.height
        self.updateViewConstraints()
    }
}


//MARK: - UITextViewDelegate

extension ComposeController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return viewModel.shouldChangeText(textView.text ?? "", in: range, replacementText: text)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        _ = viewModel.setTweetText(textView.text)
    }
}


//MARK: - Actions

extension ComposeController {
    
    @IBAction func tweetButtonPressed(_ sender: Any) {
        viewModel.sendTweet()
    }
}
