//
//  TWTRTweetView.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import TwitterKit

class TweetViewSizeCalculator {
    
    static func heightForTweet(_ tweet: TWTRTweet, style: TWTRTweetViewStyle, fittingWidth: CGFloat, showActions: Bool) -> CGFloat {
        let key = String(format: "%@:%lu:%f:%d", tweet.tweetID, Float(style.rawValue), fittingWidth, showActions)
        if let cashedHeight = self.cashedHeight[key] {
            return cashedHeight
        } else {
            let height = calculateHeight(tweet: tweet, style: style, fittingWidth: fittingWidth, showActions: showActions)
            cashedHeight[key] = height
            return height
        }
    }
    
    
    //MARK: - Private methods

    private static var cashedHeight = [String: CGFloat]()
    
    private init() {}

    private static let regularTweetView: TWTRTweetView = {
        let tweetView = TWTRTweetView(tweet: nil, style: .regular)
        return tweetView
    }()

    private static let compactTweetView: TWTRTweetView = {
        let tweetView = TWTRTweetView(tweet: nil, style: .compact)
        return tweetView
    }()
    
    private static func calculateHeight(tweet: TWTRTweet, style: TWTRTweetViewStyle, fittingWidth: CGFloat, showActions: Bool) -> CGFloat {
        let tweetView = style == .compact ? TweetViewSizeCalculator.compactTweetView : TweetViewSizeCalculator.regularTweetView
        tweetView.configure(with: tweet)
        tweetView.showActionButtons = showActions
        let size = tweetView.sizeThatFits(CGSize(width: fittingWidth, height: CGFloat(Float.greatestFiniteMagnitude)))
        return size.height
    }
}
