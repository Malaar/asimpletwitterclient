//
//  Director.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit

fileprivate struct Constants {
    
    static let mainStoryboard = "Main"
    static let authorizationStoryboard = "Authorization"
}

class Director {
    
    static let sharedInstance = Director()
    
    private init() { }

    func switchUI(isUserAuthorized: Bool) {
        let storyboardName = isUserAuthorized ? Constants.mainStoryboard : Constants.authorizationStoryboard
        let storyboard = UIStoryboard.init(name: storyboardName, bundle: nil)
        guard let controller = storyboard.instantiateInitialViewController() else {
            fatalError("Can't instantiate initial view controller from \(storyboardName) storyboard")
        }
        if let window = applicationWindow() {
            window.rootViewController = controller
        } else {
            fatalError("Application's window isn't initialized")
        }
    }
    

    //MARK: - Private methods
    
    private func applicationWindow() -> UIWindow? {
        return UIApplication.shared.delegate?.window as? UIWindow
    }
}
