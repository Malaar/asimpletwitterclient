//
//  GUITheme.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/14/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit

/**
 * App UI themes is here
 **/
struct GUITheme {
    
    static let `default` = GUITheme()
    
    private init() {}
    
    let orangeColor = UIColor(hex: "FF9B29")
    let favoriteTweetButtonColor = UIColor.red
    let unfavoriteTweetButtonColor = UIColor.gray
}
