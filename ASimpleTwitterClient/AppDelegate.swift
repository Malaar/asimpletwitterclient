//
//  AppDelegate.swift
//  ASimpleTwitterClient
//
//  Created by Malaar on 4/13/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import TwitterKit

fileprivate struct Constants {
    
    static let twitterConsumerKey = "TwitterConsumerKey"
    static let twitterConsumerSecret = "TwitterConsumerSecret"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureTwitter()
        setupUI()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }
    
    
    //MARK: - Private methods

    private func configureTwitter() {
        let infoDictionary = Bundle.main.infoDictionary
        guard let twitterConsumerKey = infoDictionary?[Constants.twitterConsumerKey] as? String, !twitterConsumerKey.isEmpty else {
            fatalError("\(Constants.twitterConsumerKey) is missed!")
        }
        guard let twitterConsumerSecret = infoDictionary?[Constants.twitterConsumerSecret] as? String, !twitterConsumerSecret.isEmpty else {
            fatalError("\(Constants.twitterConsumerSecret) is missed!")
        }

        TWTRTwitter.sharedInstance().start(withConsumerKey: twitterConsumerKey, consumerSecret: twitterConsumerSecret)
    }
    
    private func setupUI() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        Director.sharedInstance.switchUI(isUserAuthorized: TWTRTwitter.isUserAuthorized())
    }

}

