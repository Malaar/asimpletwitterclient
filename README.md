# Twitter 2.0

Twitter 2.0 is a test application, which works with Twitter API.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for overview and testing purposes.

### Prerequisites

* Cocoapods [requirements](https://cocoapods.org/)
* Mac OS X 10.13+, Xcode 9.3+

### Installing

1. Clone Twitter 2.0 repository

			git clone git@bitbucket.org:Malaar/asimpletwitterclient.git

2. Install required pods

			pod install

3. Done.

## Running

To run on iOS/mac

Navigate to /proj.ios_mac/ and open ASimpleTwitterClient.xcworkspace in Xcode

Enjoy.

## Logic details

Project was created with paradigm of Protocol Oriented Programming.
Project demonstrate work with MVVM pattern, dependency injections and other.

### Important dependencies:
* TwitterKit - for authorisation purpose
* Swifter - to work with Twitter API

## Screenshots

![Page 1](ReadmeDocs/IMG_4460.PNG)


## Unit Tests
* to simulate tweets used class `MocTwitterService`
* fake tweets loaded from file 'TweetSample.json'


## Author

* **[Andrew Korshilovskiy](http://www.linkedin.com/in/korshilovskiy)** - development

## License

(c) All rights reserved.
This project is licensed under the **Proprietary Software License** - see the [LICENSE](http://www.binpress.com/license/view/l/358023be402acff778a934083b76b86f) url for details
